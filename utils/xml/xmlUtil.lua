-- A wrapper for Paul Chakravarti's xml module. See comments in xml.lua and handler.lua for more information.

local myFolder = (...):match("(.-)[^%.]+$")

require(myFolder..'xml')
require(myFolder..'handler')

-- Parse the xml file and return the parsed file as a table
---------------------------------
-- @param xmlFile text file containing the xml to parse
---------------------------------
function parseXMLToTable(xmlFile)
  local xmlText = ""
  local xmlFileSize = 0
  local xmlhandler = simpleTreeHandler()
  local xmlparser = xmlParser(xmlhandler)

  local xmlText, xmlFileSize = love.filesystem.read(xmlFile)
  xmlparser:parse(xmlText)

  return xmlhandler.root
end