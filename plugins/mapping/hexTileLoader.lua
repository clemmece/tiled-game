-- Builds a tile set from a table containing metadata for each tile and a sprite/tilesheet image
-- Also builds a quad object for each tile
------------------------------------------------------------------
-- @param tileData table containing metadata describing each tile
-- @param tileImage image file containing the tiles
------------------------------------------------------------------
function buildTileSet(tileData, tileImage)
  local tileSet = {}
  tileSet['tileImage'] = love.graphics.newImage(tileImage)
  tileSet['tileSetW'] = tileSet['tileImage']:getWidth()
  tileSet['tileSetH'] = tileSet['tileImage']:getHeight()

  tileSet['tileSDistance'] = tileData.TextureAtlas._attr.sidelength
  tileSet['tileTDistance'] = tileData.TextureAtlas._attr.baseheight
  tileSet['oneQuarterW'] = tileSet['tileSDistance'] / 2
  tileSet['tileH'] = tileData.TextureAtlas._attr.h
  tileSet['tileW'] = tileData.TextureAtlas._attr.w

  tileSet['tiles'] = {}

  local tile = {}

  for k, v in pairs(tileData.TextureAtlas.SubTexture) do
    tile['name'] = v._attr.name
    tile['x'] = v._attr.x
    tile['y'] = v._attr.y
    tile['width'] = v._attr.width
    tile['height'] = v._attr.height
    tile['quad'] = love.graphics.newQuad(tile.x, tile.y, tile.width, tile.height, tileSet.tileImage:getDimensions())

    table.insert(tileSet.tiles, tile)
    tile = {}
  end

  return tileSet
end
