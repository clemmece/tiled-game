function drawCursor(x, y, tileWidth, tileHeight)
  local cursorX, cursorY
  if y % 2 == 0 then
    cursorX = (x * tileWidth)
    cursorY = (tileWidth * (.75 * y + .5))
  else
    cursorX = (x * tileWidth) + (tileWidth / 2)
    cursorY = tileWidth * (.75 * y + .5)
  end

  return cursorX, cursorY
end

-- The magic numbers here are specific to the hex tileset I'm using and probably
-- won't work for other tiles unless they are the same dimensions and shape.
function getTileOffsets(rowIndex, columnIndex, tileWidth)
  local vOffset = rowIndex * tileWidth * 0.75
  local hOffset = tileWidth * columnIndex

  if rowIndex % 2 == 0 then
    hOffset = hOffset + (tileWidth * .5)
  end

  return hOffset, vOffset
end
