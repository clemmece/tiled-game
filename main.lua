require 'utils.xml.xmlUtil'
require 'plugins.mapping.hexTileLoader'

-- Weird text offsets help visualize level layout
-- TileMap =  {
--   {0,3,3},
--    {9,0,1},
--   {0,1,1},
--    {0,1,1,0,0},
--   {0,0,0,0,1}
-- }

function love.update(dt)

end

function love.load()
  -- lets get it over with
  math.randomseed(os.time())

  -- monolithic state tracking object ... yay
  GameStateTable = {}
  GameObjectsTable = {}

  GameObjectsTable['map_x'] = 0
  GameObjectsTable['map_y'] = 0
  GameObjectsTable['map_w'] = 1000
  GameObjectsTable['map_h'] = 1000
  GameObjectsTable['map_display_w'] = love.graphics.getWidth()
  GameObjectsTable['map_display_h'] = love.graphics.getHeight()

  dofile('assets/maps/test_map.lua')
  TileMap = game_map

  TileSet = {}


  -- who am i?
  love.window.setTitle('Tiled Game')

  -- where's my stuff?
  -- love.filesystem.setIdentity('TiledGame')

  local loveDir = love.filesystem.getSource()
  local parsedXML = parseXMLToTable("assets/hexagonTiles/Spritesheet/fullTiles.xml")

  TileSet = buildTileSet(parsedXML, "assets/hexagonTiles/Spritesheet/fullTiles.png")

  -- require the file for dealing with the desired tileset
  local utils = require('plugins.mapping.hexTileUtils')
  TileSet['drawCursor'] = drawCursor
  TileSet['getTileOffsets'] = getTileOffsets

  selectedTile = {x=2, y=1}
end

function love.draw()
  local row, tile, number, hOffset, vOffset, cursorX, cursorY
  love.graphics.setBackgroundColor(255,255,255)

  for rowIndex=1, #TileMap do
    row = TileMap[rowIndex]
    for columnIndex = 1, #row do
      number = row[columnIndex]

      -- 0 == empty hole
      if number ~= 0 then
        tile = TileSet['tiles'][number]

        hOffset, vOffset = TileSet.getTileOffsets(rowIndex, columnIndex, tile.width)

        -- still getting texture bleed ... not sure what's going on
        love.graphics.draw(TileSet.tileImage,
                           tile.quad,
                           math.floor(hOffset + GameObjectsTable.map_x),
                           math.floor(vOffset + GameObjectsTable.map_y))
      end
    end
  end

  love.graphics.setColor(255, 0, 0)
  cursorX, cursorY = TileSet.drawCursor(selectedTile.x, selectedTile.y, TileSet.tileW, TileSet.tileH)
  love.graphics.circle("fill", cursorX, cursorY, TileSet['tileSDistance']/2, 20)

  -- back to normal
  love.graphics.reset()
end


function love.mousepressed(x, y, button, istouch)

end

function love.mousereleased(x, y, button, istouch)

end

function love.keypressed(key, unicode)
  local cursorX, cursorY = TileSet.drawCursor(selectedTile.x, selectedTile.y, TileSet.tileW, TileSet.tileH)

   if key == 'up' then
      if(selectedTile.y <= 0) then
        selectedTile.y = 0
      else
        selectedTile.y = selectedTile.y - 1
      end

      if(cursorY <= 0) then
        GameObjectsTable.map_y = GameObjectsTable.map_y + 10
      end

   end

   if key == 'down' then
      if(selectedTile.y >= map_max_height) then
        selectedTile.y = map_max_height
      else
        selectedTile.y = selectedTile.y + 1
      end

      if(cursorY >= GameObjectsTable.map_display_h) then
        GameObjectsTable.map_y = GameObjectsTable.map_y - 10
      end
   end

   if key == 'left' then
      if(selectedTile.x <= 0) then
        selectedTile.x = 0
      else
        selectedTile.x = selectedTile.x - 1
      end

      if(cursorX <= 0) then
        GameObjectsTable.map_x = GameObjectsTable.map_x + 10
      end
   end

   if key == 'right' then
      if(selectedTile.x >= map_max_width) then
        selectedTile.x = map_max_width
      else
        selectedTile.x = selectedTile.x + 1
      end

      if(cursorX >= GameObjectsTable.map_display_w - 10) then
        GameObjectsTable.map_x = GameObjectsTable.map_x - 10
      end

   end
end

function love.keyreleased(key)

end

function love.focus(f)

end

function love.quit()

end

function getTexturePositions()
  local posTable = {}

  return posTable
end
